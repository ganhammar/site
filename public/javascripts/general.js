function extend(a, b) {
  for(var key in b) {
    if(b.hasOwnProperty(key))
      a[key] = b[key];
  }

  return a;
}

Element.prototype.hide = function() {
  this.style.display = "none";
  return this;
};

Element.prototype.show = function() {
  this.style.display = '';
  return this;
};

Element.prototype.toggle = function() {
  if(this.style.display == "none")
    return this.show();
  else
    return this.hide();
};

function ajax(url, method, params, callback, async, step_callback) {
	if(async == undefined)
		async = false;

	var xmlhttp = new XMLHttpRequest();

	var result = null;
	xmlhttp.onreadystatechange = function() {
		if(step_callback !== undefined)
			step_callback(xmlhttp);

		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			result = callback(xmlhttp.responseText);
		}
	};

  if(typeof params == "object") {
    params = Object.keys(params).map(function(key) {
      return encodeURIComponent(key) + "=" + encodeURIComponent(params[key]);
    }).join("&");
  }

	xmlhttp.open(method, url, async);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(params);
	return result;
}
