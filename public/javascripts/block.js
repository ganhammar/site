var colors = [
  "#FC504E",
  "#FCCB5F",
  "#47BF71",
  "#17A3A5",
  "#225B66",
  "#D85BE9",
  "#D8F301"
];

function getColor(color) {
  return colors[color];
}

function Piece() {
   this.states = [];
   this.currentState = 0;
   this.gridX = 0;
   this.gridY = 0;
   this.colorMax = 7;

   this.color;
   this.date = new Date().getTime();
};

Piece.prototype.getColor = function() {
  if(this.color == undefined)
    this.color = Math.floor(Math.random() * this.colorMax);

  return colors[this.color];
};
Piece.prototype.getWidth = function() {
  var width = 0;
  for(var r = 0; r < this.states[this.currentState].length; r++) {
    if(this.states[this.currentState][r].length > width)
      width = this.states[this.currentState][r].length;
  }

  return width;
};
Piece.prototype.getHeight = function() {
  return this.states[this.currentState].length;
};
Piece.prototype.nextState = function() {
  this.currentState = this.getNextState();
};
Piece.prototype.previousState = function() {
  this.currentState = this.getPreviousState();
};
Piece.prototype.getNextState = function() {
  if(this.states[this.currentState + 1] != undefined)
    return this.currentState + 1;
  else
    return 0;
};
Piece.prototype.getPreviousState = function() {
  if(this.states[this.currentState - 1] != undefined)
    return this.currentState - 1;
  else
    return this.states.length - 1;
};

function LPiece() {
}

function RLPiece() {
}

function BPiece() {
}

function IPiece() {
}

function ZPiece() {
}

function RZPiece() {
}

function TPiece() {
}

LPiece.prototype = new Piece();
LPiece.prototype.states.push([
    [1, 0],
    [1, 0],
    [1, 1]
  ]);
LPiece.prototype.states.push([
    [0, 0, 1],
    [1, 1, 1]
  ]);
LPiece.prototype.states.push([
    [1, 1],
    [0, 1],
    [0, 1]
  ]);
LPiece.prototype.states.push([
    [1, 1, 1],
    [1, 0, 0]
  ]);

RLPiece.prototype = new Piece();
RLPiece.prototype.states.push([
    [0, 1],
    [0, 1],
    [1, 1]
  ]);
RLPiece.prototype.states.push([
    [1, 1, 1],
    [0, 0, 1]
  ]);
RLPiece.prototype.states.push([
    [1, 1],
    [1, 0],
    [1, 0]
  ]);
RLPiece.prototype.states.push([
    [1, 0, 0],
    [1, 1, 1]
  ]);

BPiece.prototype = new Piece();
BPiece.prototype.states.push([
    [1, 1],
    [1, 1]
  ]);

IPiece.prototype = new Piece();
IPiece.prototype.states.push([
    [1],
    [1],
    [1],
    [1]
  ]);
IPiece.prototype.states.push([
    [1, 1, 1, 1]
  ]);

ZPiece.prototype = new Piece();
ZPiece.prototype.states.push([
    [0, 1, 1],
    [1, 1, 0]
  ]);
ZPiece.prototype.states.push([
    [1, 0],
    [1, 1],
    [0, 1]
  ]);

RZPiece.prototype = new Piece();
RZPiece.prototype.states.push([
    [1, 1, 0],
    [0, 1, 1]
  ]);
RZPiece.prototype.states.push([
    [0, 1],
    [1, 1],
    [1, 0]
  ]);

TPiece.prototype = new Piece();
TPiece.prototype.states.push([
    [1, 1, 1],
    [0, 1, 0]
  ]);
TPiece.prototype.states.push([
    [1, 0],
    [1, 1],
    [1, 0]
  ]);
TPiece.prototype.states.push([
    [0, 1, 0],
    [1, 1, 1]
  ]);
TPiece.prototype.states.push([
    [0, 1],
    [1, 1],
    [0, 1]
  ]);

function getPiece() {
  var target = Math.floor(Math.random() * 7);
  var piece;

  switch(target) {
    case 0:
      piece = new LPiece();
      break;
    case 1:
      piece = new RLPiece();
      break;
    case 2:
      piece = new BPiece();
      break;
    case 3:
      piece = new ZPiece();
      break;
    case 4:
      piece = new RZPiece();
      break;
    case 5:
      piece = new IPiece();
      break;
    case 6:
      piece = new TPiece();
      break;
  }

  return piece;
}
