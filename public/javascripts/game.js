function Game(target, options) {
  var defaultOptions = {
    rows: 20,
    cols: 15,
    timePerLevel: 120000,
    timePerTick: 500,
    timePerTickModifier: 0.2,
    timePerTickLevelModifier: 0.8,
    useTimePerTickModifier: false,
    maxLevels: 10,
    size: 26,
    scoring: [
      40, 100, 300, 1200
    ],
    keys: {
      "start": {
        "keyCode": 32,
        "keyName": "space",
        "explanation": "Start the game"
      },
      "reset": {
        "keyCode": 82,
        "keyName": "R",
        "explanation": "Restart the game"
      },
      "pause": {
        "keyCode": 80,
        "keyName": "P",
        "explanation": "Pause/unpause the game"
      },
      "rotate": {
        "keyCode": 38,
        "keyName": "&uarr;",
        "explanation": "Rotate the current block"
      },
      "boost": {
        "keyCode": 40,
        "keyName": "&darr;",
        "explanation": "Hold this key down to increase the speed"
      },
      "left": {
        "keyCode": 37,
        "keyName": "&larr;",
        "explanation": "Move the current block to the left"
      },
      "right": {
        "keyCode": 39,
        "keyName": "&rarr;",
        "explanation": "Move the current block to the right"
      }
    }
  };

  this.options = extend(defaultOptions, options);

  this.canvas;
  this.context;
  this.instructions;
  this.levelElement;
  this.nicknameWrapper;
  this.scoreElement;
  this.scoringWrapper;
  this.target = target;

  this.currentTime = 0;
  this.previousTime = 0;
  this.timePerTick;
  this.timeSinceLevel = 0;
  this.timeDiffToLevelWhenPaused = 0;

  this.currentPiece;
  this.gameData;

  this.isGameOver = false;
  this.isGamePaused = true;
  this.isGameStarted = false;

  this.level = 1;
  this.score = 0;

  this.sessionId;
}

Game.prototype.init = function() {
  this.setBoardSize();

  this.context = this.canvas.getContext("2d");

  this.renderInstructions();
  this.renderScoringTable();
  this.renderNicknameForm();
  this.nicknameWrapper.hide();

  this.clearGameData();

  document.body.onkeydown = this.handleInput.bind(this);
  document.body.onkeyup = this.handleInput.bind(this);
};

Game.prototype.setBoardSize = function() {
  this.target.style.width = (this.options.cols * this.options.size) + "px";
  this.target.style.height = (this.options.rows * this.options.size) + "px";

  var canvas = document.createElement("canvas");
  canvas.width = this.options.cols * this.options.size;
  canvas.height = this.options.rows * this.options.size;

  this.target.appendChild(canvas);
  this.canvas = canvas;
};

Game.prototype.renderInstructions = function() {
  var instructions = document.createElement("div");
  instructions.id = "instructions";

  for(var i in this.options.keys) {
    instructions.appendChild(
      this.createKeyInstructionElements(
          this.options.keys[i].keyName,
          this.options.keys[i].explanation,
          i
        )
    );
  }

  this.target.appendChild(instructions);
  this.instructions = instructions;
};

Game.prototype.createKeyInstructionElements = function(key, explanation, keyClass) {
  var buttonWrapper = document.createElement("div");
  buttonWrapper.className = "keyboard-button-wrapper";

  var button = document.createElement("div");
  button.className = "keyboard-button";
  var inner = document.createElement("span");
  inner.innerHTML = key;
  inner.className = keyClass;
  button.appendChild(inner);
  buttonWrapper.appendChild(button);

  var explanationWrapper = document.createElement("span");
  explanationWrapper.className = "keyboard-button-explanation";
  explanationWrapper.appendChild(document.createTextNode(explanation));
  buttonWrapper.appendChild(explanationWrapper);

  return buttonWrapper;
};

Game.prototype.renderScoringTable = function() {
  var scoringTable = document.createElement("div");
  scoringTable.id = "scoring-table";

  var scoreWrapper = this.createScoreContent(
    "scoring-table-score", "Score", this.score
  );
  scoringTable.appendChild(
    scoreWrapper
  );
  this.scoreElement = scoreWrapper.getElementsByClassName("value")[0];

  var levelWrapper = this.createScoreContent(
    "scoring-table-level", "Level", this.level
  );
  scoringTable.appendChild(
    levelWrapper
  );
  this.levelElement = levelWrapper.getElementsByClassName("value")[0];

  this.target.appendChild(scoringTable);
  this.scoreWrapper = scoringTable;
};

Game.prototype.createScoreContent = function(wrapperId, name, value) {
  var scoreWrapper = document.createElement("div");
  scoreWrapper.id = wrapperId;

  var nameWrapper = document.createElement("span");
  nameWrapper.className = "name";
  nameWrapper.appendChild(document.createTextNode(name));
  scoreWrapper.appendChild(nameWrapper);

  var valueWrapper = document.createElement("span");
  valueWrapper.className = "value";
  valueWrapper.appendChild(document.createTextNode(value));
  scoreWrapper.appendChild(valueWrapper);

  return scoreWrapper;
};

Game.prototype.renderNicknameForm = function() {
  var nicknameWrapper = document.createElement("div");
  nicknameWrapper.id = "nickname-wrapper";

  var label = document.createElement("label");
  label.for = "nickname";
  label.appendChild(document.createTextNode(
    "Nickname (for the not yet implemented high score list)"
  ));
  nicknameWrapper.appendChild(label);

  var input = document.createElement("input");
  input.name = "nickname";
  input.type = "text";
  input.id = "nickname";
  input.maxLength = 4;
  input.onkeydown = this.handleNicknameInput.bind(this);
  nicknameWrapper.appendChild(input);

  var button = document.createElement("button");
  button.appendChild(document.createTextNode("Submit"));
  button.onclick = this.submitNicknameForm.bind(this);
  nicknameWrapper.appendChild(button);

  this.nicknameWrapper = nicknameWrapper;
  this.target.appendChild(nicknameWrapper);
};

Game.prototype.handleNicknameInput = function(event) {
  var input = document.getElementById("nickname");
};

Game.prototype.submitNicknameForm = function() {
  if(!this.sessionId)
    return;

  var input = document.getElementById("nickname");
  var self = this;
  var nickname = input.value.substring(0, 4);

  ajax("/result", "post", { nickname: nickname, id: this.sessionId }, function(data) {
    self.nicknameWrapper.hide();
  });
};

Game.prototype.start = function() {
  this.instructions.hide();
  this.resetGameVars();
  this.isGameStarted = true;
  this.isGamePaused = false;
  this.currentPiece = this.getNewPiece();
  this.timePerTick = this.options.timePerTick;
  this.timeSinceLevel = new Date().getTime();
  this.logGameStarted();

  this.startAnimation();
};

Game.prototype.logGameStarted = function() {
  var self = this;
  ajax("/result", "post", {}, function(data) {
    if(data)
      self.sessionId = data;
  });
};

Game.prototype.logGameResult = function() {
  if(!this.sessionId)
    return;

  var self = this;
  var params = {
    id: this.sessionId,
    score: this.score,
    level: this.level
  };

  ajax("/result", "post", params, function(data) {
    // Handle error
  });
};

Game.prototype.startAnimation = function() {
  window.requestAnimationFrame = window.requestAnimationFrame ||
    window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame;

  window.requestAnimationFrame(this.update.bind(this));
};

Game.prototype.reset = function() {
  this.instructions.show();
  this.resetGameVars();
  this.isGameStarted = false;
  this.isGamePaused = true;
  this.currentPiece.gridX = 0;
  this.currentPiece.gridY = - this.currentPiece.getHeight();
  this.clearGameData();
  this.context.clearRect(
    0,
    0,
    this.options.cols * this.options.size,
    this.options.rows * this.options.size
  );
};

Game.prototype.resetGameVars = function() {
  this.isGameOver = false;
  this.score = 0;
  this.level = 1;
  this.scoreWrapper.className = "";
  this.renderScore();
  this.nicknameWrapper.hide();
};

Game.prototype.clearGameData = function() {
  var r, c;

  if(this.gameData == undefined) {
    this.gameData = new Array();

    for(r = 0; r < this.options.rows; r++) {
      this.gameData[r] = new Array();

      for(c = 0; c < this.options.cols; c++) {
        this.gameData[r].push(0);
      }
    }
  }
  else {
    for(r = 0; r < this.options.rows; r++) {
      for(c = 0; c < this.options.cols; c++) {
        this.gameData[r][c] = 0;
      }
    }
  }
};

Game.prototype.handleInput = function(event) {
  if(event.type == "keydown") {
    for(var i in this.options.keys) {
      if(this.options.keys[i].keyCode == event.keyCode) {
        switch(i) {
          case "start":
            if(this.isGameStarted == false)
              this.start();
            break;
          case "left":
            if(this.isGameOver == false && this.isGameStarted == true &&
                this.isGamePaused == false) {
              if(this.currentPiece.gridX - 1 >= 0 &&
                    this.isCollision(this.currentPiece, -1) == false) {
                this.currentPiece.gridX -= 1;
              }
            }
            break;
          case "right":
            if(this.isGameOver == false && this.isGameStarted == true &&
                this.isGamePaused == false) {
              var newX = this.currentPiece.gridX + this.currentPiece.getWidth() + 1;
              if(newX <= this.options.cols &&
                  this.isCollision(this.currentPiece, 1) == false) {
                this.currentPiece.gridX += 1;
              }
            }
            break;
          case "rotate":
            if(this.isGameOver == false && this.isGameStarted == true &&
                this.isGamePaused == false) {
              var nextState = this.currentPiece.getNextState();
              if(this.isCollision(this.currentPiece, 0, 0, nextState) == false)
                this.currentPiece.nextState();
            }
            break;
          case "boost":
            if(this.isGameOver == false && this.isGameStarted == true &&
                this.isGamePaused == false) {
              this.options.useTimePerTickModifier = true;
            }
            break;
          case "pause":
            if(this.isGameOver == false && this.isGameStarted == true) {
              if(this.isGamePaused == true) {
                this.instructions.hide();
                this.isGamePaused = false;
                this.startAnimation();
                this.timeSinceLevel = new Date().getTime() -
                  this.timeDiffToLevelWhenPaused;
              }
              else {
                this.instructions.show();
                this.isGamePaused = true;
                this.timeDiffToLevelWhenPaused = new Date().getTime() -
                  this.timeSinceLevel;
              }
            }
            break;
          case "reset":
            if(this.isGameStarted == true)
              this.reset();
            break;
        }
      }
    }
  }
  else if(event.type == "keyup") {
    for(var i in this.options.keys) {
      if(this.options.keys[i].keyCode == event.keyCode) {
        switch(i) {
          case "boost": // Down
            this.options.useTimePerTickModifier = false;
            break;
        }
      }
    }
  }
};

Game.prototype.getNewPiece = function() {
  var piece = getPiece();

  piece.gridY = - piece.getHeight();
  piece.gridX = Math.ceil(this.options.cols / 2 - piece.getWidth() / 2);

  return piece;
}

Game.prototype.update = function() {
  if(this.isGamePaused == true || this.isGameStarted == false)
    return;

  this.currentTime = new Date().getTime();

  if(this.currentTime - this.timeSinceLevel > this.options.timePerLevel &&
      this.level < this.options.maxLevels) {
    this.level++;
    this.timePerTick *= this.options.timePerTickLevelModifier;
    this.timeSinceLevel = this.currentTime;
    this.renderScore();
  }

  var timePerTick = this.timePerTick;

  if(this.options.useTimePerTickModifier)
    timePerTick *= this.options.timePerTickModifier;

  if(this.currentTime - this.previousTime > timePerTick) {
    if(this.isCollision(this.currentPiece, 0, 1)) {
      this.addToGameData(this.currentPiece);

      if(this.isGameOver == false)
        this.currentPiece = this.getNewPiece();
    }
    else {
      this.currentPiece.gridY++;
    }

    this.previousTime = this.currentTime;
  }

  this.context.clearRect(
    0,
    0,
    this.options.cols * this.options.size,
    this.options.rows * this.options.size
  );
  this.renderBoard();
  this.renderPiece(this.currentPiece);

  if(this.isGameOver == false) {
    window.requestAnimationFrame(this.update.bind(this));
  }
  else {
    this.renderGameOverScreen();
    this.logGameResult();
  }
};

Game.prototype.renderGameOverScreen = function() {
  this.instructions.show();
  this.nicknameWrapper.show();
  this.instructions.className = "game-over"
  this.scoreWrapper.className = "game-over";
};

Game.prototype.renderBoard = function() {
  for(var r = 0; r < this.options.rows; r++) {
    for(var c = 0; c < this.options.cols; c++) {
      if(this.gameData[r][c] > 0) {
        this.context.fillStyle = getColor(this.gameData[r][c] - 1);
        this.context.fillRect(
          c * this.options.size,
          r * this.options.size,
          this.options.size,
          this.options.size
        );
      }
    }
  }
};

Game.prototype.renderPiece = function(piece) {
  var currentState = piece.currentState;

  for(var r = 0; r < piece.states[currentState].length; r++) {
    for(var c = 0; c < piece.states[currentState][r].length; c++) {
      if(piece.states[currentState][r][c] == 1 &&
          piece.getHeight() + piece.gridY > 0) {
        this.context.fillStyle = piece.getColor();
        this.context.fillRect(
          (piece.gridX + c) * this.options.size,
          (piece.gridY + r) * this.options.size,
          this.options.size,
          this.options.size
        );
      }
    }
  }
};

Game.prototype.isCollision = function(piece, moveX, moveY, changeState) {
  var x = piece.gridX;
  var y = piece.gridY;
  var currentState = piece.currentState;

  if(moveX != undefined)
    x += moveX;

  if(moveY != undefined)
    y += moveY;

  if(changeState != undefined)
    currentState = changeState;

  for(var r = 0; r < piece.states[currentState].length; r++) {
    for(var c = 0; c < piece.states[currentState][r].length; c++) {
      if(x + c < 0 || x + c > this.options.cols ||
          y + r >= this.options.rows) {
        return true;
      }

      if(this.gameData[y + r] != undefined &&
          this.gameData[y + r][x + c] != 0 &&
          piece.states[currentState][r] != undefined &&
          piece.states[currentState][r][c] != 0) {
        return true;
      }
    }
  }

  return false;
};

Game.prototype.addToGameData = function(piece) {
  var currentState = piece.currentState;

  for(var r = 0; r < piece.states[currentState].length; r++) {
    for(var c = 0; c < piece.states[currentState][r].length; c++) {
      if(piece.states[currentState][r][c] == 1 && piece.gridY >= 0) {
        this.gameData[piece.gridY + r][piece.gridX + c] = piece.color + 1;
      }
    }
  }

  this.checkForFilledRows();

  if(piece.gridY < 0) {
    this.isGameOver = true;
  }
};

Game.prototype.checkForFilledRows = function() {
  var rowIsFull;
  var filledRows = [];

  for(var r = this.options.rows - 1; r > 0; r--) {
    rowIsFull = true;
    for(var c = 0; c < this.options.cols; c++) {
      if(this.gameData[r][c] == 0) {
        rowIsFull = false;
        break;
      }
    }

    if(rowIsFull == true) {
      filledRows.push(r);
    }
  }

  if(filledRows.length == 0)
    return;

  this.increaseScore(filledRows);

  filledRows.reverse();

  for(var i in filledRows) {
    this.emptyRowAndReorderGameData(filledRows[i]);
  }
};

Game.prototype.increaseScore = function(rows) {
  var currentRowNum;
  var continuousRowsNum;
  var scoredRows = [];

  for(var i in rows) {
    if(scoredRows.indexOf(i) !== -1)
      continue;

    currentRowNum = rows[i];
    continuousRowsNum = 1;

    for(var x in rows) {
      if(rows[x] == currentRowNum - 1) {
        continuousRowsNum++;
        currentRowNum = rows[x];
        scoredRows.push(x);
      }
    }

    this.score += this.options.scoring[continuousRowsNum - 1] * this.level;
  }

  this.renderScore();
};

Game.prototype.renderScore = function() {
  this.scoreElement.textContent = this.score;
  this.levelElement.textContent = this.level;
};

Game.prototype.emptyRowAndReorderGameData = function(rowNum) {
  for(var r = rowNum; r > 0; r--) {
    for(var c = 0; c < this.options.cols; c++) {
      if(r - 1 > 0)
        this.gameData[r][c] = this.gameData[r - 1][c];
      else
        this.gameData[r][c] = 0;
    }
  }
};
